
		$(function () {
			$(".info__showmore").on("click", function () {
				var $this = $(this);				
				var $content = $this
					.parents(".card-info-main-right-block")
					.find("div.card-info-main-right-inner-text");
				var Text = $this.text();
				var linkText1 = $this.data("text1");
				var linkText2 = $this.data("text2");
				if (Text == linkText1) {
					$this.text(linkText2);
					$content.addClass("showContent");
					$('#innerText').css('max-height', $('#innerText p').height()); 
					$(".info__showmore").addClass("open");
				} else {
					$('#innerText').css('max-height', ''); 
					$this.text(linkText1);
					$content.removeClass("showContent");
					$(".info__showmore").removeClass("open");
				}
			});
		});

		function checkTextHeight() {
			var textHeight = $('#innerText p').height();
			var maxHeight = parseFloat($('#innerText p').css('line-height')) * 13;
			if (textHeight <= maxHeight) {
				$('.card-info-main-right-block-info').css('display', 'none');
				$('#innerText').addClass('short');
			} else {
				$('.card-info-main-right-block-info').css('display', '');
				$('#innerText').removeClass('short');
			}
		}

		checkTextHeight();

		$(window).on('resize', function() {
			checkTextHeight();
		});
    